package com.fara.android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ParcelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel);

        Button button = findViewById(R.id.buttonSonderbar);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               double total = calculatePrice();
               double volume = calculateVolume();
                Log.i("ButtonParcel","Price: "+total +"Volume: "+ volume);
                TextView printPrice = findViewById(R.id.outputParcelWeightResult);
                printPrice.setText(Double.toString(total));
            }
        });
    }


    public double calculatePrice() {
        //Gewicht wird entgegen genommen und in Double konvertiert
        EditText weight = findViewById(R.id.editTextParcelWeight);
        String value = weight.getText().toString();
        Double dblVal = Double.parseDouble(value);
        double price = 0.0;
        if (dblVal > 0 && dblVal <= 20) {
            if (dblVal <= 5) {
                price = 7.0;
            } else {
                if (dblVal <= 10) {
                    price = 10.5;
                } else {
                    price = 14.0;
                }
            }
        }
        return price;
    }

    public double calculateVolume(){
        //Paketmaße entgegen nehmen
        EditText height = findViewById(R.id.editTextParcelHeight);
        String valueHeight = height.getText().toString();
        Double parcelHeight = Double.parseDouble(valueHeight);

        EditText length = findViewById(R.id.editTextParcelLength);
        String valueLength = length.getText().toString();
        Double parcelLength = Double.parseDouble(valueLength);

        EditText width = findViewById(R.id.editTextParcelWidth);
        String valueWidth = width.getText().toString();
        Double parcelWidth = Double.parseDouble(valueWidth);
        return parcelHeight * parcelLength * parcelWidth;
    }

}