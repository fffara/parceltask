package com.fara.android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class StringsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strings);

        String txt;

        //Klasse R bildet alle Resources ab

       txt = getString(R.string.my_text);

        Log.i("Strings",txt);

        //Formatierte Zeichenketten
        double preis = 123.456;
        String waehrung = "Euro";
        txt = getString(R.string.total_price,preis,waehrung);
        Log.i("Strings",txt);
    }
}