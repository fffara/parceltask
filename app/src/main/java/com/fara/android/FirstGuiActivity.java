package com.fara.android;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FirstGuiActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_gui);

//ich kann mir die Elemente der View über id holen und weiter verarbeiten z.B. onClick soll es diese Aktion ausführen
        TextView textView = findViewById(R.id.textViewBeispiel);

        //textView.append("bla bla");
        textView.setText("bla bla");
        textView.setBackgroundColor(Color.GREEN);

        EditText editText = findViewById(R.id.editTextBeispiel);

        String eingabe = editText.getText().toString();

        Log.i("Firstgui", eingabe);

        //Event handling über separate Top-Level-Klasse

        Button button = findViewById(R.id.buttonBeispiel);
        // MeinKlickHorcher horchi = new MeinKlickHorcher();
        //objektreferenz mitgeben
        //button.setOnClickListener(horchi);

        //Event handling über die eigene Klasse/über die eigene Activity

        button.setOnClickListener(this);

        //Event handling über innere lokale anonyme Klasse
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("BUTTON", "OnClick über eine anonyme Klasse");
//            }
//        });

        //Event handling über Lambda Ausdruck
       /* button.setOnClickListener(view->ausgabe(view));
        button.setOnClickListener(view->lange());*/

    }

   public boolean lange() {
        Log.i("BUTTON", "OnClick");
        return true;
    }

    @Override
    public void onClick(View view) {
        Log.i("BUTTON", "OnClick über die eigene Klasse");
    }

    public void ausgabe(View view){
        Log.i("BUTTON", "OnClick über Lambda");
        view.setBackgroundColor(Color.RED);
        Button b = (Button) view;
        b.setText("bla bla");

        EditText editText = findViewById(R.id.editTextBeispiel);
        String eingabe = editText.getText().toString();
        Log.i("BUTTON",eingabe);
    }

    public void machwas(View view){
        Log.i("BUTTON", "ich mach was");
    }
}